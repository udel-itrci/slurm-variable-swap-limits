/***************************************************************************** \
 *  dynamic_swap_limits.c
\*****************************************************************************/

#include "dynamic_swap_limits.h"

#include <sys/sysinfo.h>


/*
 * __dynamic_swap_limit_total_swap
 *
 * Use the sysinfo() API to figure out how much swap is available on the
 * system.
 */
int
__dynamic_swap_limit_total_swap(
    uint64_t        *total_swap
)
{
    struct sysinfo  system_info_data;
    
    if ( sysinfo(&system_info_data) == 0 ) {
        *total_swap = system_info_data.totalswap * system_info_data.mem_unit;
        info("maximum swap calculated from sysinfo is %"PRIu64, *total_swap);
        return 1;
    }
    return 0;
}


/*
 * __dynamic_swap_limit_memory_parse
 *
 * Parse a byte count, possibly with a unit on it.
 */
static int
__dynamic_swap_limit_memory_parse(
    size_t              base_index,
    const char          *start,
    uint64_t            *out_byte_count,
    const char*         *out_end
)
{
    int                 rc = 0;
    char                *endp = NULL;
    unsigned long long  value = strtoull(start, &endp, 0), magnitude = 0, base = 1000;
    
    if ( endp > start ) {
        while ( *endp && isspace(*endp) ) endp++;
        switch ( *endp ) {
            case 'T':
                magnitude++;
            case 'P':
                magnitude++;
            case 'G':
                magnitude++;
            case 'M':
                magnitude++;
            case 'K':
            case 'k':
                magnitude++;
                endp++;
            default:
                break;
        }
        if ( *endp && (*endp == 'I' || *endp == 'i') ) {
            base = 1024;
            endp++;
        }
        if ( *endp && (*endp == 'B' || *endp == 'b') ) endp++;
        
        while ( magnitude-- ) value *= base;
        
        if ( out_byte_count ) *out_byte_count = value;
        if ( out_end ) *out_end = endp;
        rc = 1;
    } else {
        error("unable to parse byte count at %llu", base_index);
    }
    return rc;
}

/**/

typedef struct dynamic_swap_limit_properties {
    uint64_t            min_swap, max_swap;
    uint32_t            swappiness;
} dynamic_swap_limit_properties_t;


static int
__dynamic_swap_limit_propertires_is_default(
    dynamic_swap_limit_properties_t *properties
)
{
    return (properties->min_swap == NO_VAL64) &&
           (properties->max_swap == NO_VAL64) &&
           (properties->swappiness == NO_VAL);
}


static char*
__dynamic_swap_limit_properties_printable(
    dynamic_swap_limit_properties_t *properties
)
{
    char            *out_str = NULL;
    
    if ( ! __dynamic_swap_limit_propertires_is_default(properties) ) {
        int         need_delim = 0;
        
        out_str = xstrdup("{");
        if ( properties->max_swap != NO_VAL64 ) {
            xstrfmtcat(out_str, "max_swap:%"PRIu64, properties->max_swap);
            need_delim = 1;
        }
        if ( properties->min_swap != NO_VAL64 ) {
            xstrfmtcat(out_str, "%smin_swap:%"PRIu64, (need_delim ? "," : ""), properties->min_swap);
            need_delim = 1;
        }
        if ( properties->swappiness != NO_VAL ) {
            xstrfmtcat(out_str, "%sswappiness:%"PRIu32, (need_delim ? "," : ""), properties->min_swap);
            need_delim = 1;
        }
        xstrcat(out_str, "}");
    }
    return out_str;
}



static void
__dynamic_swap_limit_properties_init(
    dynamic_swap_limit_properties_t *properties
)
{
    properties->min_swap = NO_VAL64;
    properties->max_swap = NO_VAL64;
    properties->swappiness = NO_VAL;
}

static int
__dynamic_swap_limit_properties_parse(
    size_t                          base_index,
    const char                      *start,
    const char*                     *out_end,
    dynamic_swap_limit_properties_t *properties
)
{
    const char                      *s = start;
    int                             okay = 1;
    
    if ( *s == '{' ) {
        s++;
        
        while ( *s && (*s != '}') ) {
            const char              *prop_start = s, *prop_end = s;
            const char              *val_start, *val_end;
            
            while ( *prop_end && (*prop_end != ':') ) prop_end++;
            if ( prop_end == prop_start ) {
                error("zero-length property name is invalid at %llu", (unsigned long long)(base_index + s - start));
                okay = 0;
                break;
            }
            val_start = prop_end;
            if ( *val_start == ':' ) val_start++;
            val_end = val_start;
            while ( *val_end && ((*val_end != ',') && (*val_end != '}')) ) val_end++;
            
            /* We've isolated the property name and value, go ahead and parse
             * them now:
             */
            if ( xstrncasecmp(prop_start, "max_swap", prop_end - prop_start) == 0 ) {
                uint64_t            max_swap;
                
                /* Parse the maximum swap byte count: */
                if ( ! __dynamic_swap_limit_memory_parse(base_index + val_start - start, val_start, &max_swap, NULL) ) {
                    okay = 0;
                    error("unable to parse max_swap:%s", val_start);
                    break;
                }
                properties->max_swap = max_swap;
            }
            else if ( xstrncasecmp(prop_start, "min_swap", prop_end - prop_start) == 0 ) {
                uint64_t            min_swap;
                
                /* Parse the maximum swap byte count: */
                if ( ! __dynamic_swap_limit_memory_parse(base_index + val_start - start, val_start, &min_swap, NULL) ) {
                    okay = 0;
                    error("unable to parse min_swap:%s", val_start);
                    break;
                }
                properties->min_swap = min_swap;
            }
            else if ( xstrncasecmp(prop_start, "swappiness", prop_end - prop_start) == 0 ) {
                char                *endp = NULL;
                long int            swappiness = strtol(val_start, &endp, 0);

                if ( endp == val_start ) {
                    okay = 0;
                    error("unable to parse swappiness:%s", val_start);
                    break;
                }
                /* Range is [0,100]: */
                properties->swappiness = (swappiness < 0) ? 0 : ((swappiness > 100) ? 100 : swappiness);
            }
            else {
                error("unhandled property name at %llu", (unsigned long long)(base_index + prop_start - start));
                okay = 0;
                break;
            }
            
            if ( *val_end == ',' ) val_end++;
            s = val_end;
        }
        if ( *s == '}' ) s++;
    }        
    if ( out_end ) *out_end = s;
    return okay;
}

/**/

/*
 * dynamic_swap_limit_match_t
 *
 * There are several matching functions available in the dynamic
 * swap limits language.
 *
 *   - default          match anything
 *   - partition(X)     X is a fnmatch pattern, matches against a job's
 *                      final chosen partition
 *   - host(X)          X is a Slurm hostlist, matches against the node on
 *                      which the memsw limit is being calculated
 *
 */
typedef enum {
    dynamic_swap_limit_match_unknown = 0,
    dynamic_swap_limit_match_default,
    dynamic_swap_limit_match_partition,
    dynamic_swap_limit_match_host,
    dynamic_swap_limit_match_max
} dynamic_swap_limit_match_t;

/*
 * A list of __dynamic_swap_limit_match_strings
 *
 * The matching functions' textual forms; used for parsing and for unparsing
 * the term list.
 *
 */
struct dynamic_swap_limit_match_string {
    const char      *s;
    size_t          s_len;
};
static struct dynamic_swap_limit_match_string __dynamic_swap_limit_match_strings[dynamic_swap_limit_match_max] = {
        { "unknown", 7 },
        { "default", 7 },
        { "partition", 9 },
        { "host", 4 }
    };

/*
 * __dynamic_swap_limit_match_parse
 *
 * Attempt to parse a matching function in the C string located at start.  The
 * base_index is the location of start within the overall term list string (for
 * the sake of citing error indices).  If a valid matching function is found,
 * the enumerated id is returned, and:
 *
 *   - if out_end is not NULL, *out_end is set to point to the first character
 *     beyond the matching function specification
 *   - if out_args is not NULL, *out_args is set to point to the first character
 *     of the matching function argument list
 *   - if out_args_len is not NULL, *out_args_len is set to the number of
 *     characters in the matching function argument list
 *
 * If parsing fails, dynamic_swap_limit_match_unknown is returned.
 *
 */
static dynamic_swap_limit_match_t
__dynamic_swap_limit_match_parse(
    size_t              base_index,
    const char          *start,
    const char*         *out_end,
    const char*         *out_args,
    size_t              *out_args_len
)
{
    dynamic_swap_limit_match_t  out_match = dynamic_swap_limit_match_unknown,
                                tmp_match = dynamic_swap_limit_match_default;
    const char                  *s = start;
    
    /* Find out which matching function is indicated: */
    while ( tmp_match < dynamic_swap_limit_match_max ) {
        if ( (xstrncasecmp(s, __dynamic_swap_limit_match_strings[tmp_match].s, __dynamic_swap_limit_match_strings[tmp_match].s_len) == 0) &&
            (*(s + __dynamic_swap_limit_match_strings[tmp_match].s_len) == '(') )
        {
            /* Set s to the first character beyond the opening "(" of the
             * matching function and exit the loop -- the start of the
             * argument list to the matching function:
             */
            s += __dynamic_swap_limit_match_strings[tmp_match].s_len + 1;
            break;
        }
        tmp_match++;
    }
    if ( tmp_match != dynamic_swap_limit_match_max) {
        const char      *arg_end = s;
        
        /* Isolate the argument list to the matching function: */
        while ( *arg_end && (*arg_end != ')') ) arg_end++;
        if ( *arg_end == ')' ) {
            /* Success! */
            out_match = tmp_match;
            if ( out_args ) *out_args = s;
            if ( out_args_len ) *out_args_len = arg_end - s;
            /* Reposition start to be just past the closing ")" of the
             * matching function:
             */
            start = arg_end + 1;
        } else {
            error("unable to parse DynamicSwapLimits expression:  unterminated argument list at %llu",
                    (unsigned long long)(base_index + arg_end - start)
                );
        }
    } else {
        error("unable to parse DynamicSwapLimits expression:  bad matching function at %llu",
                (unsigned long long)base_index
            );
    }
    if ( out_end ) *out_end = start;
    return out_match;
}

/**/

/*
 * dynamic_swap_limit_unit_t
 *
 * Matching function formulas may have units associated with them.
 * The unit is an int treated as a bit vector.
 *
 * The unit type is in the lowest 4 bits:
 *
 *   - none:     special case, implies NO swap limit was specified
 *   - percent:  the formula is a percentage of the available swap
 *               (as configured in Slurm)
 *   - byte:     the formula is a byte count
 *
 * The modifier is an SI (or binary) prefix defining a multiplier
 * for the base numeric value and occurs in the next set of 5 bits.
 *
 * There are two additional modifiers:
 *
 *   - per-cpu:  the formula value should be multiplied by the
 *               number of CPUs allocated to the job on the node
 *   - per-task: the formula value should be multiplied by the
 *               number of tasks allocated to the job on the node
 *
 */
typedef enum {
    dynamic_swap_limit_unit_unknown             = 0,
    
    dynamic_swap_limit_unit_type_mask           = 0x00f,
    dynamic_swap_limit_unit_type_none           = 0x001,
    dynamic_swap_limit_unit_type_percent        = 0x002,
    dynamic_swap_limit_unit_type_byte           = 0x003,
    
    dynamic_swap_limit_unit_modifier_mask       = 0x0f0,
    dynamic_swap_limit_unit_modifier_kilo       = 0x010,
    dynamic_swap_limit_unit_modifier_mega       = 0x020,
    dynamic_swap_limit_unit_modifier_giga       = 0x030,
    dynamic_swap_limit_unit_modifier_tera       = 0x040,
    dynamic_swap_limit_unit_modifier_peta       = 0x050,
    
    dynamic_swap_limit_unit_is_binary_base      = 0x100,
    dynamic_swap_limit_unit_is_per_cpu          = 0x200,
    dynamic_swap_limit_unit_is_per_task         = 0x400,
} dynamic_swap_limit_unit_t;

/*
 * __dynamic_swap_limit_unit_parse
 *
 * Parse a formula unit in the C string at start.  The base_index is
 * the location of start within the overall term list string (for
 * the sake of citing error indices).  If a valid unit is found, the
 * enumerated id is returned, and:
 *
 *   - if out_end is not NULL, then *out_end is set to point to the first
 *     character beyond the unit specification
 */
static dynamic_swap_limit_unit_t
__dynamic_swap_limit_unit_parse(
    size_t              base_index,
    const char          *start,
    const char*         *out_end
)
{
    dynamic_swap_limit_unit_t   out_unit = dynamic_swap_limit_unit_unknown,
                                tmp_unit = dynamic_swap_limit_unit_unknown;
    const char                  *s = start;
    
    if ( ! *s || (*s == ',') ) {
        /* If no unit was provided, then we default to bytes: */
        return dynamic_swap_limit_unit_type_byte;
    }
    else if ( *s == '%' ) {
        /* Percentage */
        tmp_unit = dynamic_swap_limit_unit_type_percent;
        s++;
    }
    else {
        /* See if there's an SI prefix to consume: */
        tmp_unit = dynamic_swap_limit_unit_type_byte;
        switch ( *s ) {
            case 'P':
                tmp_unit |= dynamic_swap_limit_unit_modifier_tera;
                s++; break;
            case 'T':
                tmp_unit |= dynamic_swap_limit_unit_modifier_tera;
                s++; break;
            case 'G':
                tmp_unit |= dynamic_swap_limit_unit_modifier_giga;
                s++; break;
            case 'M':
                tmp_unit |= dynamic_swap_limit_unit_modifier_mega;
                s++; break;
            case 'K':
            case 'k':
                tmp_unit |= dynamic_swap_limit_unit_modifier_kilo;
                s++; break;
        }
        /* Is the unit base-10 or base-2? */
        if ( (*s == 'i') || (*s == 'I') ) {
            tmp_unit |= dynamic_swap_limit_unit_is_binary_base;
            s++;
        }
        /* Consume the training b/B if it's there: */
        if ( (*s == 'b') || (*s == 'B') ) s++;
    }
    if ( tmp_unit != dynamic_swap_limit_unit_unknown ) {
        if ( *s == '/' ) {
            /* There's a per-something modifier: */
            s++;
            if ( (xstrncasecmp(s, "cpu", 3) == 0) && ( ! *(s + 3) || (*(s + 3) == ',') ) ) {
                s += 3;
                tmp_unit |= dynamic_swap_limit_unit_is_per_cpu;
            }
            else if ( (xstrncasecmp(s, "core", 4) == 0) && ( ! *(s + 4) || (*(s + 4) == ',') ) ) {
                s += 4;
                tmp_unit |= dynamic_swap_limit_unit_is_per_cpu;
            }
            else if ( (xstrncasecmp(s, "task", 4) == 0) && ( ! *(s + 4) || (*(s + 4) == ',') ) ) {
                s += 4;
                tmp_unit |= dynamic_swap_limit_unit_is_per_task;
            }
        }
        if ( ! *s || (*s == ',') ) {
            /* Success! */
            out_unit = tmp_unit;
            start = s;
        } else {
            error("unable to parse DynamicSwapLimits unit at %llu",
                    (unsigned long long)(base_index + s - start)
                );
        }
    }
    if ( out_end ) *out_end = start;
    return out_unit;
}

/*
 * dynamic_swap_limit_formula_t
 *
 * A swap limit formula combines a floating-point value with a unit.
 *
 */
typedef struct {
    double                      real_number;
    dynamic_swap_limit_unit_t   unit;
} dynamic_swap_limit_formula_t;

/*
 * __dynamic_swap_limit_formula_parse
 *
 * Parse a formula -- floating-point value and unit -- from the C string at
 * start.  The base_index is the location of start within the overall term
 * list string (for the sake of citing error indices).  If a valid formula
 * is found:
 *
 *   - if out_formula is not NULL, then *out_formula is set to the parsed
 *     formula
 *   - if out_end is not NULL, then *out_end is set to a pointer to the
 *     first character beyond the formula
 *   - the function returns non-zero
 *
 * If parsing fails, the function returns zero.
 */
static int
__dynamic_swap_limit_formula_parse(
    size_t                          base_index,
    const char                      *start,
    dynamic_swap_limit_formula_t    *out_formula,
    const char*                     *out_end
)
{
    int                 rc = 0;
    
    if ( (xstrncasecmp(start, "none", 4) == 0) && (! *(start + 4) || (*(start + 4) == ',')) ) {
        /* The special value "none" -- for no limit whatsoever: */
        rc = 1;
        if ( out_formula ) {
            out_formula->real_number = 0.0f;
            out_formula->unit = dynamic_swap_limit_unit_type_none;
        }
        start += 4;
    } else {
        const char      *endp;
        double          value = strtod(start, (char**)&endp);
    
        if ( endp > start ) {
            dynamic_swap_limit_unit_t   unit;
        
            while ( isspace(*endp) ) endp++;
            unit = __dynamic_swap_limit_unit_parse(base_index + (endp - start), endp, &endp);
            if ( unit != dynamic_swap_limit_unit_unknown ) {
                rc = 1;
                if ( out_formula ) {
                    out_formula->real_number = value;
                    out_formula->unit = unit;
                }
                start = endp;
            }
        } else {
            error("unable to parse DynamicSwapLimits formula at %llu",
                    (unsigned long long)base_index
                );
        }
    }
    if ( out_end ) *out_end = start;
    return rc;
}

/*
 * __dynamic_swap_limit_formula_compute
 *
 * Given a swap formula and the maximum swap, ntasks, and ncpus for the
 * job on this node, evaluate the formula and return the computed swap
 * size.
 *
 */
static uint64_t
__dynamic_swap_limit_formula_compute(
    dynamic_swap_limit_formula_t    *formula,
    uint64_t                        max_swap,
    uint32_t                        ntasks_on_node,
    uint32_t                        ncpus_on_node
)
{
    uint64_t                        computed_swap = NO_VAL64;
    
    if ( formula ) {
        switch ( formula->unit & dynamic_swap_limit_unit_type_mask ) {
        
            case dynamic_swap_limit_unit_type_none:
                debug("compute formula:  no swap limit enforced");
                break;
            
            case dynamic_swap_limit_unit_type_percent:
                if ( max_swap == NO_VAL64 ) {
                    /* If swap was effectively not given a max, then a percentage is
                     * pointless and we just return the NO_VAL64 value:
                     */
                    warn("no maximum swap size indicated, %.1lf%% ignored and 'unlimited' used", formula->real_number);
                    computed_swap = NO_VAL64;
                } else {
                    computed_swap = (uint64_t)floor(0.01 * formula->real_number * max_swap);
                    debug("compute formula:  base percentage %.1lf%% => %"PRIu64, formula->real_number, computed_swap);
                }
                break;
            
            case dynamic_swap_limit_unit_type_byte: {
                double      tmp_computed_swap = formula->real_number;
                double      factor = ((formula->unit & dynamic_swap_limit_unit_is_binary_base) ? 1024.0 : 1000.0);
                
                switch ( formula->unit & dynamic_swap_limit_unit_modifier_mask ) {
                    case dynamic_swap_limit_unit_modifier_peta:
                        tmp_computed_swap *= factor;
                    case dynamic_swap_limit_unit_modifier_tera:
                        tmp_computed_swap *= factor;
                    case dynamic_swap_limit_unit_modifier_giga:
                        tmp_computed_swap *= factor;
                    case dynamic_swap_limit_unit_modifier_mega:
                        tmp_computed_swap *= factor;
                    case dynamic_swap_limit_unit_modifier_kilo:
                        tmp_computed_swap *= factor;
                    default:
                        break;
                }
                computed_swap = (uint64_t)floor(tmp_computed_swap);
                debug("compute formula:  base byte count %"PRIu64, computed_swap);
                break;
            }
        
        }
        if ( formula->unit & dynamic_swap_limit_unit_is_per_cpu ) {
            computed_swap *= ncpus_on_node;
            debug("compute formula:  base byte count * %u CPUs = %"PRIu64, ncpus_on_node, computed_swap);
        }
        else if ( formula->unit & dynamic_swap_limit_unit_is_per_task ) {
            computed_swap *= ntasks_on_node;
            debug("compute formula:  base byte count * %u tasks = %"PRIu64, ntasks_on_node, computed_swap);
        }
    }
    return computed_swap;
}

/**/

typedef struct dynamic_swap_limit_term {
    struct dynamic_swap_limit_term_list     *parent_list;
    struct dynamic_swap_limit_term          *link;
    
    dynamic_swap_limit_properties_t         properties;
    
    dynamic_swap_limit_match_t              match;
    void                                    *match_args;
    dynamic_swap_limit_formula_t            formula;
} dynamic_swap_limit_term_t;

/*
 * __dynamic_swap_limit_term_alloc
 *
 * Allocate and initialize a dynamic_swap_limit_term node with the provided
 * matching function, argument list, and formula.
 *
 * Returns NULL on error.
 *
 */
static dynamic_swap_limit_term_t*
__dynamic_swap_limit_term_alloc(
    dynamic_swap_limit_match_t      match,
    const char                      *match_args,
    size_t                          match_args_len,
    dynamic_swap_limit_formula_t    formula,
    dynamic_swap_limit_properties_t *properties
)
{
    dynamic_swap_limit_term_t       *new_term = (dynamic_swap_limit_term_t*)xmalloc(sizeof(dynamic_swap_limit_term_t));
    
    if ( new_term ) {
        new_term->parent_list = NULL;
        new_term->link = NULL;
        
        /* Copy the parsed properties into place or init with defaults: */
        if ( properties ) {
            new_term->properties = *properties;
        } else {
            __dynamic_swap_limit_properties_init(&new_term->properties);
        }
        
        new_term->match = match;
        if ( match_args && match_args_len ) {
            switch ( match ) {
                case dynamic_swap_limit_match_unknown:
                case dynamic_swap_limit_match_max: {
                    /* Code coverage -- should never be reached */
                    error("cannot create term node from unknown matching function");
                    xfree(new_term);
                    return NULL;
                }
                case dynamic_swap_limit_match_default:
                    break;
                case dynamic_swap_limit_match_host: {
                    char            *hostlist_str = xstrndup(match_args, match_args_len);
                    
                    new_term->match_args = slurm_hostlist_create(hostlist_str);
                    xfree(hostlist_str);
                    break;
                }
                case dynamic_swap_limit_match_partition: {
                    int     rc;
                    
                    new_term->match_args = xstrndup(match_args, match_args_len);
                    switch ( rc = fnmatch((const char*)new_term->match_args, "", 0) ) {
                        case 0:
                        case FNM_NOMATCH:
                            break;
                        default:
                            error("invalid fnmatch pattern: %s (rc=%d)", (const char*)new_term->match_args, rc);
                            xfree(new_term->match_args);
                            xfree(new_term);
                            return NULL;
                    }
                    break;
                }
            }
        } else {
            new_term->match_args = NULL;
        }
        new_term->formula = formula;
    }
    return new_term;
}

static void
__dynamic_swap_limit_term_dealloc(
    dynamic_swap_limit_term_t   *term,
    int                         should_dealloc_children
)
{   
    do {
        dynamic_swap_limit_term_t   *next = term->link;
        
        if ( term->match_args ) {
            switch ( term->match ) {
                case dynamic_swap_limit_match_unknown:
                case dynamic_swap_limit_match_max:
                case dynamic_swap_limit_match_default:
                    /* Code coverage -- should never be reached */
                    break;
                case dynamic_swap_limit_match_host:
                    slurm_hostlist_destroy(term->match_args);
                    break;
                case dynamic_swap_limit_match_partition:
                    xfree(term->match_args);
                    break;
            }
        }
        xfree(term);
        term = next;
    } while ( should_dealloc_children && term );
}
    
/**/

typedef struct dynamic_swap_limit_term_list {
    dynamic_swap_limit_properties_t default_properties;
    
    dynamic_swap_limit_term_t       *list_head;
} dynamic_swap_limit_term_list_t;




dynamic_swap_limit_term_list_ref
dynamic_swap_limit_term_list_parse(
    const char  *specification
)
{
    dynamic_swap_limit_term_list_t  *out_term_list = NULL;
    dynamic_swap_limit_properties_t default_props;
    dynamic_swap_limit_term_t       *head = NULL, *last = NULL;
    size_t                          s_index = 0;
    const char                      *s = specification;
    const char                      *endp = NULL;
    
    debug("parsing spec string '%s'", specification);
    
    /* Check for the default properties first: */
    __dynamic_swap_limit_properties_init(&default_props);
    if ( ! __dynamic_swap_limit_properties_parse(s_index, s, &endp, &default_props) ) {
        error("error parsing default properties: '%s'", specification);
        return NULL;
    }
    s_index += (endp - s);
    s = endp;
    
    while ( s && *s ) {
        /* At the current position in the string, attempt to parse a matching
         * function:
         */
        const char                  *the_args = NULL;
        size_t                      the_args_len = 0;
        dynamic_swap_limit_match_t  the_match = __dynamic_swap_limit_match_parse(
                                                    s_index,
                                                    s,
                                                    &endp,
                                                    &the_args,
                                                    &the_args_len
                                                );
        if ( the_match != dynamic_swap_limit_match_unknown ) {
            dynamic_swap_limit_properties_t     the_properties;
            
            debug3("    matching function %d...", the_match);
            __dynamic_swap_limit_properties_init(&the_properties);
            if ( *endp == '{' ) {
                s_index += (endp - s);
                s = endp;
                if ( ! __dynamic_swap_limit_properties_parse(s_index, s, &endp, &the_properties) ) {
                    error("error parsing matching function properties at %llu in '%s'", (unsigned long long)s_index, specification);
                    if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
                    return NULL;
                }
            }
            if ( *endp == '=' ) {
                /* Okay, valid matching function: */
                dynamic_swap_limit_formula_t    the_formula;
                
                /* Reposition s past the "=" delimiter -- which should be the
                 * formula accompanying the matching function:
                 */
                s_index += (endp - s + 1);
                s = endp + 1;
                /* Attempt to parse a formula: */
                if ( __dynamic_swap_limit_formula_parse(s_index, s, &the_formula, &endp) ) {
                    dynamic_swap_limit_term_t   *new_term;
                    
                    /* Skip past any trailing whitespace and comma(s): */
                    while ( *endp && (isspace(*endp) || (*endp == ',')) ) endp++;
                    s_index += (endp - s);
                    
                    debug3("    formula %f[%04X]...", the_formula.real_number, the_formula.unit);
                    
                    /* Allocate a new term: */
                    new_term = __dynamic_swap_limit_term_alloc(the_match, the_args, the_args_len, the_formula, &the_properties);
                    if ( new_term ) {
                        if ( get_log_level() >= LOG_LEVEL_DEBUG ) {
                            char    *as_string = dynamic_swap_limit_term_printable(new_term, NULL);
                            debug("added term '%s'", as_string);
                            xfree(as_string);
                        }
                        if ( ! head ) head = new_term;
                        if ( last ) last->link = new_term;
                        last = new_term;
                    } else {
                        if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
                        error("failed to allocate term node at %llu in '%s'", (unsigned long long)s_index, specification);
                        return NULL;
                    }
                } else {
                    /* Invalid formula: */
                    if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
                    return NULL;
                }
            } else {
                if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
                error("no formula associated with matching function at %llu in '%s'", (unsigned long long)(s_index + (endp - s)), specification);
                return NULL;
            }
        } else {
            /* Invalid matching function: */
            if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
            return NULL;
        }
        s = endp;
    }
    
    /* The term list has been built, now allocate and return the wrapper object: */
    out_term_list = xmalloc(sizeof(dynamic_swap_limit_term_list_t));
    if ( out_term_list ) {
        out_term_list->default_properties = default_props;
        out_term_list->list_head = head;
        
        /* Now loop through the list and set parent_list on each node: */
        while ( head ) {
            head->parent_list = out_term_list;
            head = head->link;
        }
    } else {
        if ( head ) __dynamic_swap_limit_term_dealloc(head, 1);
        error("failed to allocate term list object for '%s'", specification);
    }
    return out_term_list;
}


void
dynamic_swap_limit_term_list_dealloc(
    dynamic_swap_limit_term_list_ref    term_list
)
{
    if ( term_list->list_head ) __dynamic_swap_limit_term_dealloc(term_list->list_head, 1);
    xfree(term_list);
}


dynamic_swap_limit_term_ref
dynamic_swap_limit_term_list_match(
    dynamic_swap_limit_term_list_ref    term_list,
    const char                          *hostname,
    const char                          *partition
)
{
    dynamic_swap_limit_term_ref         out_term = term_list->list_head;
    int                                 found = 0;
    
    debug("locating match for host='%s' and partition='%s'", hostname, partition);
    while ( ! found && out_term ) {
        switch ( out_term->match ) {
            case dynamic_swap_limit_match_unknown:
            case dynamic_swap_limit_match_max:
                /* Code coverage -- should never be reached */
                break;
            case dynamic_swap_limit_match_default: {
                /* Of course the default matches: */
                found = 1;
                debug("    match with default term");
                break;
            }
            case dynamic_swap_limit_match_partition: {
                if ( out_term->match_args && partition ) {
                    /* Use fnmatch() and the match_args against the partition name: */
                    int         rc;
                    switch ( rc = fnmatch((const char*)out_term->match_args, partition, 0) ) {
                        case 0: {
                            found = 1;
                            if ( get_log_level() >= LOG_LEVEL_DEBUG ) {
                                char    *as_string = dynamic_swap_limit_term_printable(out_term, NULL);
                                debug("    match with partition term '%s'", as_string);
                                xfree(as_string);
                            }
                        }
                        case FNM_NOMATCH:
                            break;
                        default:
                            error("invalid fnmatch pattern: %s (rc=%d)", (const char*)out_term->match_args, rc);
                            break;
                    }
                }
                break;
            }
            case dynamic_swap_limit_match_host: {
                if ( out_term->match_args && hostname ) {
                    /* Use match_args as a Slurm hostlist and test for membership of hostname in that
                     * list:
                     */
                    found =(slurm_hostlist_find(out_term->match_args, hostname) != -1);
                    if ( found && (get_log_level() >= LOG_LEVEL_DEBUG) ) {
                        char    *as_string = dynamic_swap_limit_term_printable(out_term, NULL);
                        debug("    match with host term '%s'", as_string);
                        xfree(as_string);
                    }
                }
                break;
            }
        }
        if ( ! found ) out_term = out_term->link;
    }
    return out_term;
}


char*
dynamic_swap_limit_term_list_printable(
    dynamic_swap_limit_term_list_ref    term_list,
    char                                *out_string
)
{
    dynamic_swap_limit_term_ref         term = term_list->list_head;
    char                                *properties = __dynamic_swap_limit_properties_printable(&term_list->default_properties);
    int                                 need_delim = 0, xfree_properties = 1;
    
    if ( ! out_string ) {
        if ( properties ) {
            out_string = properties;
            xfree_properties = 0;
        } else {
            out_string = xstrdup("");
        }
    } else if ( properties ) {
        xstrcat(out_string, properties);
    }
    while ( term ) {
        if ( need_delim ) xstrcat(out_string, ",");
        out_string = dynamic_swap_limit_term_printable(term, out_string);
        need_delim = 1;
        term = term->link;
    }
    if ( properties && xfree_properties ) xfree(properties);
    return out_string;
}


char*
dynamic_swap_limit_term_printable(
    dynamic_swap_limit_term_ref         term,
    char                                *out_string
)
{
    char                        *match_args = "";
    int                         xfree_match_args = 0;
    char                        *properties = __dynamic_swap_limit_properties_printable(&term->properties);
    
    if ( ! out_string ) out_string = xstrdup("");
    
    switch ( term->match ) {
        case dynamic_swap_limit_match_unknown:
        case dynamic_swap_limit_match_max:
            /* Code coverage -- should never be reached */
            break;
        case dynamic_swap_limit_match_default:
            break;
        case dynamic_swap_limit_match_host:
            if ( term->match_args ) {
                match_args = slurm_hostlist_ranged_string_xmalloc(term->match_args);
                xfree_match_args = 1;
            }
            break;
        case dynamic_swap_limit_match_partition:
            match_args = term->match_args;
            break;
    }
    xstrfmtcat(out_string, "%s(%s)%s=%lf[%04X]", __dynamic_swap_limit_match_strings[term->match].s, match_args, (properties ? properties : ""), term->formula.real_number, term->formula.unit);
    if ( xfree_match_args ) xfree(match_args);
    if ( properties ) xfree(properties);
    return out_string;
}


uint64_t
dynamic_swap_limit_term_compute(
    dynamic_swap_limit_term_ref         term,
    uint32_t                            ntasks_on_node,
    uint32_t                            ncpus_on_node
)
{
    uint64_t                out_limit = NO_VAL64;
    
    if ( term ) {
        uint64_t            max_swap = NO_VAL64, min_swap = 0;
        
        if ( term->properties.max_swap != NO_VAL64 ) max_swap = term->properties.max_swap;
        else if ( term->parent_list->default_properties.max_swap != NO_VAL64 ) max_swap = term->parent_list->default_properties.max_swap;
        else {
            __dynamic_swap_limit_total_swap(&max_swap);
        }
        
        if ( term->properties.min_swap != NO_VAL64 ) min_swap = term->properties.min_swap;
        else if ( term->parent_list->default_properties.min_swap != NO_VAL64 ) min_swap = term->parent_list->default_properties.min_swap;
        
        /* min_swap cannot exceed max_swap: */
        if ( min_swap > max_swap ) {
            min_swap = max_swap;
            info("minimum swap capped at max_swap = %"PRIu64, min_swap);
        }
        if ( max_swap != NO_VAL64 ) debug("maximum swap chosen is %"PRIu64, max_swap);
        
        /* Calculate swap limit for this job: */
        out_limit = __dynamic_swap_limit_formula_compute(&term->formula, max_swap, ntasks_on_node, ncpus_on_node);
        
        /* Ensure min_swap is met: */
        if ( out_limit < min_swap ) {
            debug("calculated swap size below minimum, defaulting to %"PRIu64, min_swap);
            out_limit = min_swap;
        } else {
            debug("calculated swap size %"PRIu64, out_limit);
        }
    }
    return out_limit;
}


int
dynamic_swap_limit_term_get_property(
    dynamic_swap_limit_term_ref     term,
    dynamic_swap_limit_property_t   property,
    int                             should_return_term_only,
    ...
)
{
    int         rc = 1;
    va_list     vargs;
    
    va_start(vargs, should_return_term_only);
    switch ( property ) {
        case dynamic_swap_limit_property_max_swap: {
            uint64_t        value = term->properties.max_swap;
            
            if ( (value == NO_VAL64) && ! should_return_term_only ) value = term->parent_list->default_properties.max_swap;
            *(va_arg(vargs, uint64_t*)) = value;
            break;
        }
        case dynamic_swap_limit_property_min_swap: {
            uint64_t        value = term->properties.min_swap;
            
            if ( (value == NO_VAL64) && ! should_return_term_only ) value = term->parent_list->default_properties.min_swap;
            *(va_arg(vargs, uint64_t*)) = value;
            break;
        }
        case dynamic_swap_limit_property_swappiness: {
            uint32_t        value = term->properties.swappiness;
            
            if ( (value == NO_VAL) && ! should_return_term_only ) value = term->parent_list->default_properties.swappiness;
            *(va_arg(vargs, uint32_t*)) = value;
            break;
        }
        default:
            rc = 0;
            break;
    }
    va_end(vargs);
    return rc;
}


int
dynamic_swap_limit_term_set_property(
    dynamic_swap_limit_term_ref     term,
    dynamic_swap_limit_property_t   property,
    ...
)
{
    int         rc = 1;
    va_list     vargs;
    
    va_start(vargs, property);
    switch ( property ) {
        case dynamic_swap_limit_property_max_swap:
            term->properties.max_swap = va_arg(vargs, uint64_t);
            break;
        case dynamic_swap_limit_property_min_swap:
            term->properties.min_swap = va_arg(vargs, uint64_t);
            break;
        case dynamic_swap_limit_property_swappiness:
            term->properties.swappiness = va_arg(vargs, uint32_t);
            break;
        default:
            rc = 0;
            break;
    }
    va_end(vargs);
    return rc;
}

