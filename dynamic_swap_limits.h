/***************************************************************************** \
 *  dynamic_swap_limits.h
\*****************************************************************************/

#ifndef __DYNAMIC_SWAP_LIMITS_H__
#define __DYNAMIC_SWAP_LIMITS_H__

#define _GNU_SOURCE		/* For POLLRDHUP, O_CLOEXEC on older glibc */
#include <limits.h>
#include <poll.h>
#include <signal.h>
#include <stdlib.h>		/* getenv */
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <fnmatch.h>

#include "slurm/slurm_errno.h"
#include "slurm/slurm.h"
#include "src/slurmd/slurmstepd/slurmstepd_job.h"
#include "src/slurmd/slurmd/slurmd.h"

#include "src/common/xstring.h"

/**********************************

<wildcard> ::= "*"

<partition-character> ::= <letter> | <digit> | "_" | <wildcard>

<host-character> ::= <letter> | <number> | "-" | "_" | "."

<property> ::= 1*( <letter> | <digit> | "." )

<value> ::= *( <letter> | <digit> | "." )

<term-list> ::= ( "{" *( <property> ":" <value> *( "," <property> ":" <value> ) ) "}" )? <term> *( "," <term> )

  <term> ::= <match> "=" <formula> | "none"

    <match> ::= ( "default()" | <partition-expr> | <host-expr> ) ( "{" *( <property> ":" <value> *( "," <property> ":" <value> ) ) "}" )?

      <partition-expr> ::= "partition(" 1*<partition-character> ")"

      <host-expr> ::= "host(" <host-pattern> ")"

        <host-pattern> ::= ( <hostname> | <hostlist> ) *( "," ( <hostname> | <hostlist> ) )

          <hostname> ::= <letter> *<host-character>

          <hostlist> ::= *<host-character> ( <numeric-pattern> *<host-character> )

            <numeric-pattern> ::= "[" ( <numeric-fragment> | <numeric-range> ) *( "," ( <numeric-fragment> | <numeric-range> ) ) "]"

              <numeric-fragment> ::= 1*<digit>

              <numeric-range> ::= <numeric-fragment> "-" <numeric-fragment>
                        
    <formula> =  <real-number> [ <unit> [ "/cpu" | "/core" | "/task" ] ]
    
      <real-number> ::= *<digit> [ "." *<digit> ] [ ( "e" | "E" ) ( "+" | "-" ) *<digit> ]
        
      <unit> ::= "%" | ( ( "P" | "T" | "G" | "M" | "K" | "k" ) [ "i" ] ) [ "B" ]
      
 **********************************/

/*
 * dynamic_swap_limit_term_list_ref
 *
 * An opaque reference to a swap limit term list.
 */
typedef struct dynamic_swap_limit_term_list *dynamic_swap_limit_term_list_ref;

/*
 * dynamic_swap_limit_term_ref
 *
 * An opaque reference to a swap limit term.
 */
typedef struct dynamic_swap_limit_term *dynamic_swap_limit_term_ref;

/*
 * dynamic_swap_limit_term_list_parse
 *
 * Attempt to parse a swap limit term list specification string.  Returns NULL
 * in case of any failure.  Caller is responsible for disposing of the returned
 * term list using dynamic_swap_limit_term_list_dealloc().
 */
extern dynamic_swap_limit_term_list_ref dynamic_swap_limit_term_list_parse(const char *specification);

/*
 * dynamic_swap_limit_term_list_dealloc
 *
 * Dispose of a swap limit term list returned by dynamic_swap_limit_term_list_parse().
 */
extern void dynamic_swap_limit_term_list_dealloc(dynamic_swap_limit_term_list_ref term_list);

/*
 * dynamic_swap_limit_term_list_printable
 *
 * Generate a textual representation of the given term_list.  If out_string is NULL, then
 * a new C string is allocated; otherwise, the text is appended to out_string.
 *
 * Returns NULL on error; out_string if a non-NULL out_string was passed to the function;
 * or a new C string allocated by the function.
 */
extern char* dynamic_swap_limit_term_list_printable(dynamic_swap_limit_term_list_ref term_list, char *out_string);

/*
 * dynamic_swap_limit_term_list_match
 *
 * Given a hostname and partition name, attempt to locate the first term in term_list whose
 * matching function responds to either of those strings.
 *
 * Returns NULL if no matching term is found.
 */
extern dynamic_swap_limit_term_ref dynamic_swap_limit_term_list_match(dynamic_swap_limit_term_list_ref term_list, const char *hostname, const char *partition);

/*
 * dynamic_swap_limit_term_printable
 *
 * Generate a textual representation of the given swap limit term (as returned by the
 * dynamic_swap_limit_term_list_match() function).  If out_string is NULL, then
 * a new C string is allocated; otherwise, the text is appended to out_string.
 *
 * Returns NULL on error; out_string if a non-NULL out_string was passed to the function;
 * or a new C string allocated by the function.
 */
extern char* dynamic_swap_limit_term_printable(dynamic_swap_limit_term_ref term, char *out_string);

/*
 * dynamic_swap_limit_term_compute
 *
 * Given a swap limit term returned by dynamic_swap_limit_term_list_match(), calculate the
 * swap limit given a task and CPU count associated with a job.
 *
 * Returns NO_VAL64 if the result is "unlimited."
 */
extern uint64_t dynamic_swap_limit_term_compute(dynamic_swap_limit_term_ref term, uint32_t ntasks_on_node, uint32_t ncpus_on_node);

/*
 * dynamic_swap_limit_property_t
 *
 * An enumeration of known properties; used to retrieve and set their values on terms.
 *
 *     dynamic_swap_limit_property_max_swap         uint64_t
 *     dynamic_swap_limit_property_min_swap         uint64_t
 *     dynamic_swap_limit_property_swappiness       uint32_t
 *
 */
typedef enum {
    dynamic_swap_limit_property_max_swap,
    dynamic_swap_limit_property_min_swap,
    dynamic_swap_limit_property_swappiness
} dynamic_swap_limit_property_t;

/*
 * dynamic_swap_limit_term_get_property
 *
 * Given a swap limit term, retrieve the given property.  If should_return_term_only is false
 * and the term lacks a set value for the desired property, the default value of the property
 * from the term's parent term list will be returned.
 *
 * The fourth argument is a pointer to an appropriate variable in which to store the property
 * value.  See the dynamic_swap_limit_property_t documentation for types.
 *
 * Returns zero if any failure occurs, non-zero otherwise.
 */
extern int dynamic_swap_limit_term_get_property(dynamic_swap_limit_term_ref term, dynamic_swap_limit_property_t property, int should_return_term_only, ...);

/*
 * dynamic_swap_limit_term_set_property
 *
 * Given a swap limit term, set the given property value.
 *
 * The third argument is an appropriate value for the property.  See the dynamic_swap_limit_property_t
 * documentation for types.
 *
 * Returns zero if any failure occurs, non-zero otherwise.
 */
extern int dynamic_swap_limit_term_set_property(dynamic_swap_limit_term_ref term, dynamic_swap_limit_property_t property, ...);

#endif /* __DYNAMIC_SWAP_LIMITS_H__ */
