/***************************************************************************** \
 *  dynamic_swap_limits.c
\*****************************************************************************/

#include "dynamic_swap_limits.h"
#include <getopt.h>
#include "src/common/log.h"


static struct option cli_options[] = {
   { "help",            no_argument,         0,  'h' },
   { "hostname",        required_argument,   0,  'H' },
   { "partition",       required_argument,   0,  'p' },
   { "ntasks",          required_argument,   0,  'n' },
   { "cpus-per-task",   required_argument,   0,  'c' },
   { NULL,              0,                   0,   0  }
};

const char *cli_options_str = "+hH:p:n:c:";

void
usage(
    const char      *argv0
)
{
    printf(
            "usage:\n\n"
            "    %s {options} <specification> {{options} <specification> ..}\n\n"
            "  options:\n\n"
            "    -h/--help                  display this text\n"
            "    -H/--hostname <name>       hostname to use for matching\n"
            "    -p/--partition <name>      partition name to use for matching\n"
            "    -n/--ntasks #              number of local tasks\n"
            "    -c/--cpus-per-task #       number of local CPUs\n"
            "\n"
            "  <specification>\n"
            "     * the term 'ibid' reuses the last-parsed term list\n"
            "     * otherwise, a swap limit term list to be parsed\n"
            "\n",
            argv0
        );
}

int
main(
    int             argc,
    const char*     argv[]
)
{
    int                                 rc = 0, argi = 1;
    const char                          *hostname = "r00n00", *partition = "devel";
    uint32_t                            ntasks = 5, cpus_per_task=2;
    dynamic_swap_limit_term_list_ref    term_list = NULL;
    log_options_t                       log_conf = { LOG_LEVEL_DEBUG5,  LOG_LEVEL_QUIET, LOG_LEVEL_QUIET, 1, 0 };
    
    log_init((char*)argv[0], log_conf, SYSLOG_FACILITY_DAEMON, NULL);
    while ( argi < argc ) {
        int     optc;
        
        do {
            switch ( (optc = getopt_long(argc, (char**)argv, cli_options_str, cli_options, NULL)) ) {
        
                case 'h': {
                    usage(argv[0]);
                    exit(0);
                }
            
                case 'H': {
                    hostname = optarg ? optarg : "r00n00";
                    break;
                }
            
                case 'p': {
                    partition = optarg ? optarg : "devel";
                    break;
                }
            
                case 'n': {
                    if ( optarg ) {
                        char        *endp;
                        long int    value = strtol(optarg, &endp, 0);
                    
                        ntasks = ((value > 0) && (endp > optarg)) ? value : 5;
                    } else {
                        ntasks = 5;
                    }
                    break;
                }
            
                case 'c': {
                    if ( optarg ) {
                        char        *endp;
                        long int    value = strtol(optarg, &endp, 0);
                    
                        cpus_per_task = ((value > 0) && (endp > optarg)) ? value : 5;
                    } else {
                        cpus_per_task = 2;
                    }
                    break;
                }
            }
        } while ( optc != -1 );
        
        argi = optind;
        
        while ( argi < argc ) {
            uint32_t                            ncpus = ntasks * cpus_per_task;
            
            if ( argv[argi][0] == '-' ) break;
            
            if ( strcasecmp(argv[argi], "ibid") != 0 ) {
                if ( term_list ) dynamic_swap_limit_term_list_dealloc(term_list);
                term_list = dynamic_swap_limit_term_list_parse(argv[argi]);
            }
            if ( term_list ) {
                char    *term_list_str = dynamic_swap_limit_term_list_printable(term_list, NULL);
            
                if ( term_list_str ) {
                    printf("Yields:  %s\n", term_list_str);
                    xfree(term_list_str);
                }
            
                dynamic_swap_limit_term_ref the_term = dynamic_swap_limit_term_list_match(term_list, hostname, partition);
                if ( the_term ) {
                    printf("Compute(hostname=\"%s\", partition=\"%s\", ntasks=%d, ncpus=%d) = %llu\n",
                            hostname, partition, ntasks, ncpus,
                            (unsigned long long)dynamic_swap_limit_term_compute(the_term, ntasks, ncpus)
                        );
                }
            }
            argi++;
        }
        optind = argi;
    }
    if ( term_list ) dynamic_swap_limit_term_list_dealloc(term_list);
    return rc;
}
