# Slurm Variable Swap Limits

SPANK plugin that implements finer-grain per-job swap limits.


## Build the plugin

The CMake build system generator is used to configure:

```
$ pwd
<prefix>/slurm-variable-swap-limits
$ mkdir build-20211119
$ cd build-20211119
$ cmake -DSLURM_PREFIX=/opt/shared/slurm/17.11.8 \
        -DSLURM_SOURCE_DIR=/opt/shared/slurm/17.11.8/src \
        -DSLURM_BUILD_DIR=/opt/shared/slurm/17.11.8/src/build \
    ..
CMake Deprecation Warning at CMakeLists.txt:1 (CMAKE_MINIMUM_REQUIRED):
  Compatibility with CMake < 2.8.12 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.


-- The C compiler identification is GNU 4.8.5
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Found SLURM: /opt/shared/slurm/17.11.8/lib/slurm/libslurmfull.so  
-- Configuring done
-- Generating done
-- Build files have been written to: /opt/shared/slurm/add-ons/slurm-variable-swap-limits/build-20211119
```

The software is built using `make`:

```
$ make
[ 16%] Building C object CMakeFiles/dynamic_swap_limits_test.dir/dynamic_swap_limits.c.o
[ 33%] Building C object CMakeFiles/dynamic_swap_limits_test.dir/dynamic_swap_limits_test.c.o
[ 50%] Linking C executable dynamic_swap_limits_test
[ 50%] Built target dynamic_swap_limits_test
[ 66%] Building C object CMakeFiles/dynamic_swap_limits.dir/dynamic_swap_limits.c.o
[ 83%] Building C object CMakeFiles/dynamic_swap_limits.dir/dynamic_swap_limits_plugin.c.o
[100%] Linking C shared module dynamic_swap_limits.so
[100%] Built target dynamic_swap_limits
```

The `dynamic_swap_limits_test` executable can be used to test the swap limit term matching and computation; the `--help` flag describes its usage.

```
$ ./dynamic_swap_limits_test --help
usage:

    ./dynamic_swap_limits_test {options} <specification> {{options} <specification> ..}

  options:

    -h/--help                  display this text
    -H/--hostname <name>       hostname to use for matching
    -p/--partition <name>      partition name to use for matching
    -n/--ntasks #              number of local tasks
    -c/--cpus-per-task #       number of local CPUs

  <specification>
     * the term 'ibid' reuses the last-parsed term list
     * otherwise, a swap limit term list to be parsed

$ ./dynamic_swap_limits_test -n2 -c2 -H n000 -p standard '{max_swap:10GiB}host(n001){max_swap:30TiB}=5%/cpu,default()=1%/cpu' -H n001 ibid
dynamic_swap_limits_test: debug:  parsing spec string '{max_swap:10GiB}host(n001){max_swap:30TiB}=5%/cpu,default()=1%/cpu'
dynamic_swap_limits_test: debug3:     matching function 3...
dynamic_swap_limits_test: debug3:     formula 5.000000[0202]...
dynamic_swap_limits_test: debug:  added term 'host(n001){max_swap:33776997205278720}=5.000000[0202]'
dynamic_swap_limits_test: debug3:     matching function 1...
dynamic_swap_limits_test: debug3:     formula 1.000000[0202]...
dynamic_swap_limits_test: debug:  added term 'default()=1.000000[0202]'
Yields:  {max_swap:10737418240}host(n001){max_swap:33776997205278720}=5.000000[0202],default()=1.000000[0202]
dynamic_swap_limits_test: debug:  locating match for host='n000' and partition='standard'
dynamic_swap_limits_test: debug:      match with default term
dynamic_swap_limits_test: debug:  maximum swap chosen is 10737418240
dynamic_swap_limits_test: debug:  compute formula:  base percentage 1.0% => 107374182
dynamic_swap_limits_test: debug:  compute formula:  base byte count * 10 CPUs = 1073741820
dynamic_swap_limits_test: debug:  calculated swap size 1073741820
Compute(hostname="n000", partition="standard", ntasks=2, ncpus=4) = 1073741820
Yields:  {max_swap:10737418240}host(n001){max_swap:33776997205278720}=5.000000[0202],default()=1.000000[0202]
dynamic_swap_limits_test: debug:  locating match for host='n001' and partition='standard'
dynamic_swap_limits_test: debug:      match with host term 'host(n001){max_swap:33776997205278720}=5.000000[0202]'
dynamic_swap_limits_test: debug:  maximum swap chosen is 33776997205278720
dynamic_swap_limits_test: debug:  compute formula:  base percentage 5.0% => 1688849860263936
dynamic_swap_limits_test: debug:  compute formula:  base byte count * 10 CPUs = 16888498602639360
dynamic_swap_limits_test: debug:  calculated swap size 16888498602639360
Compute(hostname="n001", partition="standard", ntasks=2, ncpus=4) = 16888498602639360
```

The plugin can be installed using `make install`.
