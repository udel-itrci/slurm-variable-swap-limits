/*
 * dynamic_swap_limits_plugin
 *
 * SLURM SPANK plugin that alters job virtual memory limits.
 */

#include "dynamic_swap_limits.h"

#include <slurm/spank.h>

#include "src/slurmd/common/xcgroup.h"

/*
 * The memory cgroup has an upper limit of PAGE_COUNTER_MAX * PAGE_SIZE:
 */
uint64_t
__dynamic_swap_limits_mem_cgroup_max(void)
{
    uint64_t        max = INT64_MAX;
    int             page_size = getpagesize();
    
    max /= page_size;
    return (max * page_size);
}

#ifndef DYNAMIC_SWAP_LIMITS_DEFAULT_SWAPPINESS
#   define DYNAMIC_SWAP_LIMITS_DEFAULT_SWAPPINESS 10
#endif

/*
 * All spank plugins must define this macro for the SLURM plugin loader.
 */
SPANK_PLUGIN(dynamic_swap_limits, 1)

#undef DYNAMIC_SWAP_LIMITS_NEED_INTERACTIVE_AND_PENDING_STEPS
#undef DYNAMIC_SWAP_LIMITS_NEED_CGROUP_CONF

#if SLURM_VERSION_MAJOR(SLURM_VERSION_NUMBER) > 18

#   define XCGROUP_NS_CREATE(A,B,C) xcgroup_ns_create(A, B, C)

#   if SLURM_VERSION_MAJOR(SLURM_VERSION_NUMBER) >= 20 && SLURM_VERSION_MINOR(SLURM_VERSION_NUMBER) >= 11
#       define DYNAMIC_SWAP_LIMITS_NEED_INTERACTIVE_AND_PENDING_STEPS
#   endif

#else

#   define DYNAMIC_SWAP_LIMITS_NEED_CGROUP_CONF
#   define XCGROUP_NS_CREATE(A,B,C) xcgroup_ns_create(&slurm_cgroup_conf, A, B, C)
    static int slurm_cgroup_conf_is_inited = 0;
    static slurm_cgroup_conf_t slurm_cgroup_conf;

    extern char*
    xcgroup_create_slurm_cg(xcgroup_ns_t* ns)
    {
        char* pre = (char*)xstrdup(slurm_cgroup_conf.cgroup_prepend);
#   ifdef MULTIPLE_SLURMD
        if ( conf->node_name != NULL )
                xstrsubstitute(pre,"%n", conf->node_name);
        else {
                xfree(pre);
                pre = (char*) xstrdup("/slurm");
        }
#   endif
        return pre;
    }

#endif


/*
 * __dynamic_swap_limits_default_swappiness
 *
 * Read the default swappiness on the system.
 */
int
__dynamic_swap_limits_default_swappiness(
    uint32_t            *out_swappiness
)
{
    int                 rc = 1;
    FILE                *fptr = fopen("/proc/sys/vm/swappiness", "r");
    
    if ( fptr ) {
        unsigned int    swappiness;
        
        if ( fscanf(fptr, "%u", &swappiness) == 1 ) {
            *out_swappiness = swappiness;
            rc = 0;
        }
        fclose(fptr);
    }
    return rc;
}


/*
 * __dynamic_swap_limits_getenv_uint32
 *
 * Attempt to retrieve a given job environment variable and convert to an integer.
 */
spank_err_t
__dynamic_swap_limits_getenv_uint32(
    spank_t         spank_ctxt,
    const char      *envvar_name,
    uint32_t        *value
)
{
    char            *value_str = getenv(envvar_name);
    spank_err_t     rc = ESPANK_ENV_NOEXIST;
    
    if ( value_str && *value_str ) {
        char            *endptr;
        unsigned long   i = strtoul(value_str, &endptr, 10);
        
        if ( endptr > value_str ) {
            if ( value ) *value = (uint32_t)i;
            rc = ESPANK_SUCCESS;
        } else {
            rc = 100 + ESPANK_BAD_ARG;
        }
    }
    return rc;
}

/*
 * @function slurm_spank_init
 *
 * Basically a no-op.
 */
int
slurm_spank_init(
    spank_t         spank_ctxt,
    int             argc,
    char            *argv[]
)
{
    debug3("enter slurm_spank_init()");
    
#ifdef DYNAMIC_SWAP_LIMITS_NEED_CGROUP_CONF
    if (read_slurm_cgroup_conf(&slurm_cgroup_conf)) {
        error("unable to read cgroup configuration");
        return SLURM_ERROR;
    }
    slurm_cgroup_conf_is_inited = 1;
#endif
    return ESPANK_SUCCESS;
}

/*
 * @function slurm_spank_exit
 *
 * Basically a no-op.
 */
int
slurm_spank_exit(
    spank_t         spank_ctxt,
    int             argc,
    char            *argv[]
)
{
    debug3("enter slurm_spank_exit()");

#ifdef DYNAMIC_SWAP_LIMITS_NEED_CGROUP_CONF
    if ( slurm_cgroup_conf_is_inited ) free_slurm_cgroup_conf(&slurm_cgroup_conf);
#endif

    return ESPANK_SUCCESS;
}

/*
 * @function slurm_spank_task_post_fork
 *
 * Parse the plugstack configuration to produce a swap limit term list to be used
 * to determine a swap limit for the job/job step.
 *
 * If a valid configuration is present, the hostname and partition name associated
 * with the job are used to find a matching term and compute a swap limit.  The
 * slurmd xcgroup API is then used to effect the limit.
 */
int
slurm_spank_task_post_fork(
    spank_t         spank_ctxt,
    int             argc,
    char            *argv[]
)
{
    int                                     rc = ESPANK_SUCCESS;
    
    debug3("enter slurm_spank_task_post_fork");

#ifdef DYNAMIC_SWAP_LIMITS_NEED_CGROUP_CONF
    if ( ! slurm_cgroup_conf_is_inited ) {
        error("no cgroup configuration present");
        return SLURM_ERROR;
    }
#endif

    if ( spank_remote(spank_ctxt) ) {
        dynamic_swap_limit_term_list_ref    configed_term_list = NULL;
        int                                 argi = 0;
        
        /* Attempt to parse a term list: */
        while ( argi < argc ) {
            dynamic_swap_limit_term_list_ref    term_list = dynamic_swap_limit_term_list_parse(argv[argi]);
            
            if ( term_list ) {
                if ( configed_term_list ) dynamic_swap_limit_term_list_dealloc(term_list);
                configed_term_list = term_list;
                rc = ESPANK_SUCCESS;
            } else {
                rc = ESPANK_BAD_ARG;
            }
            argi++;
        }

        if ( configed_term_list && (rc == ESPANK_SUCCESS) ) {
            xcgroup_ns_t                    memory_ns;
            dynamic_swap_limit_term_ref     matched_term = NULL;
            uid_t                           jobuid;
            uint32_t                        jobid, jobstepid;
            uint32_t                        ntasks_step, ncpus_step, cpus_per_task_step;
            char                            partition[1024], hostname[1024];
            
            /* initialize memory cgroup namespace */
            if (XCGROUP_NS_CREATE(&memory_ns, "", "memory") != XCGROUP_SUCCESS) {
                error("unable to create memory namespace. "
                    "You may need to set the Linux kernel option "
                    "cgroup_enable=memory (and reboot), or disable "
                    "ConstrainRAMSpace in cgroup.conf.");
                return SLURM_ERROR;
            }
            
            /* Grab the task and cpu counts: */
        
            debug("retrieving job info from environment/context");

            if ( spank_get_item(spank_ctxt, S_JOB_ID, &jobid) != ESPANK_SUCCESS ) {
                error("unable to fetch S_JOB_ID");
                rc = ESPANK_ERROR; goto early_exit;
            }
        
            if ( spank_get_item(spank_ctxt, S_JOB_STEPID, &jobstepid) != ESPANK_SUCCESS ) {
                error("unable to fetch S_JOB_STEPID");
                rc = ESPANK_ERROR; goto early_exit;
            }
            /* If this is the EXTERN step, then don't do anything else: */
            if ( jobstepid == SLURM_EXTERN_CONT ) {
                debug("no swap changes for job %u.extern", jobid);
                goto early_exit;
            }
        
            if ( spank_get_item(spank_ctxt, S_JOB_UID, &jobuid) != ESPANK_SUCCESS ) {
                error("unable to fetch S_JOB_UID");
                rc = ESPANK_ERROR; goto early_exit;
            }

            if ( spank_get_item(spank_ctxt, S_JOB_LOCAL_TASK_COUNT, &ntasks_step) != ESPANK_SUCCESS ) {
                error("unable to fetch S_JOB_LOCAL_TASK_COUNT");
                rc = ESPANK_ERROR; goto early_exit;
            }
            if ( spank_get_item(spank_ctxt, S_STEP_CPUS_PER_TASK, &cpus_per_task_step) != ESPANK_SUCCESS ) {
                error("unable to fetch S_STEP_CPUS_PER_TASK");
                rc = ESPANK_ERROR; goto early_exit;
            }
        
            /* Calculate ncpus on node for step: */
            ncpus_step = ntasks_step * cpus_per_task_step;
        
            /* Find the hostname and partition: */
            if ( spank_getenv(spank_ctxt, "SLURM_JOB_PARTITION", partition, sizeof(partition)) ) *partition = '\0';
            if ( gethostname_short(hostname, sizeof(hostname)) ) *hostname = '\0';
        
            debug("=> partition = '%s', hostname = '%s', ntasks_step = %u, ncpus_step = %u", partition, hostname, ntasks_step, ncpus_step);
        
            /* Find a match for the partition and hostname: */
            matched_term = dynamic_swap_limit_term_list_match(configed_term_list, hostname, partition);
            if ( matched_term ) {
                uint64_t            calculated_swap = dynamic_swap_limit_term_compute(matched_term, ntasks_step, ncpus_step), limit_in_bytes;
                uint64_t            calculated_memsw_limit, job_memsw_limit, step_phys_limit;
                uint32_t            swappiness;
                int                 printlen;
                char                *mem_cgroup_base = xcgroup_create_slurm_cg(&memory_ns);
                char                user_cgroup_path[PATH_MAX], job_cgroup_path[PATH_MAX], jobstep_cgroup_path[PATH_MAX];
                char                step_id_str[64];
                xcgroup_t           job_cg, jobstep_cg;
                
                if ( ! dynamic_swap_limit_term_get_property(matched_term, dynamic_swap_limit_property_swappiness, 1, &swappiness) || (swappiness == NO_VAL) ) {
                    if ( ! __dynamic_swap_limits_default_swappiness(&swappiness) ) swappiness = DYNAMIC_SWAP_LIMITS_DEFAULT_SWAPPINESS;
                }
                debug("swappiness for job %"PRIu32, swappiness);
                
                if ( snprintf(user_cgroup_path, sizeof(user_cgroup_path), "%s/uid_%u", mem_cgroup_base, jobuid) >= PATH_MAX) {
                    error("unable to build uid %u cgroup relative path : %m", jobuid);
                    xfree(mem_cgroup_base);
                    rc = ESPANK_ERROR; goto early_exit;
                }
                if ( snprintf(job_cgroup_path, sizeof(job_cgroup_path), "%s/job_%u", user_cgroup_path, jobid) >= PATH_MAX) {
                    error("unable to build job %u cgroup relative path : %m", jobid);
                    xfree(mem_cgroup_base);
                    rc = ESPANK_ERROR; goto early_exit;
                }
                switch ( jobstepid ) {
                    case SLURM_BATCH_SCRIPT:
                        printlen = snprintf(jobstep_cgroup_path, sizeof(jobstep_cgroup_path), "%s/step_batch", job_cgroup_path);
                        break;
                    case SLURM_EXTERN_CONT:
                        printlen = snprintf(jobstep_cgroup_path, sizeof(jobstep_cgroup_path), "%s/step_extern", job_cgroup_path);
                        break;
#ifdef DYNAMIC_SWAP_LIMITS_NEED_INTERACTIVE_AND_PENDING_STEPS
                    case SLURM_INTERACTIVE_STEP:
                        printlen = snprintf(jobstep_cgroup_path, sizeof(jobstep_cgroup_path), "%s/step_interactive", job_cgroup_path);
                        break;
                    case SLURM_PENDING_STEP:
                        printlen = snprintf(jobstep_cgroup_path, sizeof(jobstep_cgroup_path), "%s/step_TBD", job_cgroup_path);
                        break;
#endif
                    default:
                        printlen = snprintf(jobstep_cgroup_path, sizeof(jobstep_cgroup_path), "%s/step_%u", job_cgroup_path, jobstepid);
                        break;
                }
                if ( printlen >= PATH_MAX) {
                    error("unable to build job step %u cgroup relative path : %m", jobstepid);
                    xfree(mem_cgroup_base);
                    rc = ESPANK_ERROR; goto early_exit;
                }
                xfree(mem_cgroup_base);
            
                /* We have all the cgroup paths, begin by creating the helper contexts: */
                if (xcgroup_create(&memory_ns, &job_cg, job_cgroup_path, 0, 0) != XCGROUP_SUCCESS) {
                    error("unable to create job %u cgroup", jobid);
                    rc = SLURM_ERROR; goto early_exit;
                }
                if (xcgroup_create(&memory_ns, &jobstep_cg, jobstep_cgroup_path, 0, 0) != XCGROUP_SUCCESS) {
                    xcgroup_destroy(&job_cg);
                    error("unable to create job %u.%u cgroup", jobid, jobstepid);
                    rc = SLURM_ERROR; goto early_exit;
                }
                
                /* What's the existing virtual limit for the job? */
                if ( xcgroup_get_uint64_param(&job_cg, "memory.memsw.limit_in_bytes", &job_memsw_limit) != XCGROUP_SUCCESS ) {
                    error("unable to get existing virtual memory limit for job %u", jobid);
                    xcgroup_destroy(&job_cg);
                    xcgroup_destroy(&jobstep_cg);
                    rc = SLURM_ERROR; goto early_exit;
                }
                
                /* What's the existing physical RAM limit for the job step? */
                if ( xcgroup_get_uint64_param(&jobstep_cg, "memory.limit_in_bytes", &step_phys_limit) != XCGROUP_SUCCESS ) {
                    error("unable to get existing physical RAM limit for job step %u.%u", jobid, jobstepid);
                    xcgroup_destroy(&job_cg);
                    xcgroup_destroy(&jobstep_cg);
                    rc = SLURM_ERROR; goto early_exit;
                }
                
                /* Virtual limit for the job step: */
                calculated_memsw_limit = __dynamic_swap_limits_mem_cgroup_max();
                if ( calculated_swap < __dynamic_swap_limits_mem_cgroup_max() ) {
                    if ( step_phys_limit < __dynamic_swap_limits_mem_cgroup_max() ) {
                        uint64_t            new_value = calculated_swap + step_phys_limit;
                        
                        if ( (new_value > calculated_swap) && (new_value < __dynamic_swap_limits_mem_cgroup_max()) ) calculated_memsw_limit = new_value;
                    }
                }
                
                /* Is the new virtual limit larger than the job's existing virtual limit? 
                 * If so, then we bump the job virtual limit to infinity and rely on the
                 * step limits to rein-in usage:
                 */
                if ( calculated_memsw_limit > job_memsw_limit ) {
                    if ( xcgroup_set_uint64_param(&job_cg, "memory.memsw.limit_in_bytes", __dynamic_swap_limits_mem_cgroup_max()) != XCGROUP_SUCCESS ) {
                        error("unable to set max virtual memory limit for job %u", jobid);
                        xcgroup_destroy(&job_cg);
                        xcgroup_destroy(&jobstep_cg);
                        rc = SLURM_ERROR; goto early_exit;
                    }
                    info("set max virtual memory limit for job %u", jobid);
                }
                
                xcgroup_destroy(&job_cg);
                
                /* Set the virtual limit for the step: */
                if ( xcgroup_set_uint64_param(&jobstep_cg, "memory.memsw.limit_in_bytes", calculated_memsw_limit) != XCGROUP_SUCCESS ) {
                    error("unable to set virtual memory limit for job step %u.%u", jobid, jobstepid);
                    xcgroup_destroy(&jobstep_cg);
                    rc = SLURM_ERROR; goto early_exit;
                }
                info("set virtual memory limit %"PRIu64" for job step %u.%u", calculated_memsw_limit, jobid);
                
                /* Set the swappiness: */
                if ( xcgroup_set_uint32_param(&jobstep_cg, "memory.swappiness", swappiness) != XCGROUP_SUCCESS ) {
                    error("unable to set swappiness for job step %u.%u", jobid, jobstepid);
                    xcgroup_destroy(&jobstep_cg);
                    rc = SLURM_ERROR; goto early_exit;
                }
                info("set swappiness %"PRIu32" for job step %u.%u", swappiness, jobid, jobstepid);
                
                xcgroup_destroy(&jobstep_cg);  
            }
            
early_exit:
            xcgroup_ns_destroy(&memory_ns);
        } else if ( rc != ESPANK_SUCCESS ) {
            warn("unable to parse a swap limit term list");
        }
        if ( configed_term_list ) dynamic_swap_limit_term_list_dealloc(configed_term_list);
    }
    return rc;
}
